docker exec -e CONTEXT=$CONTEXT -e ORGANIZATION=$ORGANIZATION -i seeder sh -c 'scripts/setup-e2e.sh'
docker exec -e CONTEXT=$CONTEXT -e ORGANIZATION=$ORGANIZATION -i playwright sh -c 'scripts/playwright.sh'
