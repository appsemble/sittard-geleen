# Production

### Table of contents

- [Description](#description)
- [Certificates and secrets](#certificates-and-secrets)
- [Production](#production)

### Description

**The production app is discontinued.**

The document describes the information relevant to the production version of the app.

### Production

Running the app in production requires the
[security policy](https://gitlab.com/appsemble/apps/sittard-geleen/-/blob/386b17bfe2de496d7262ed24f7f4286007546dae/apps/bevolkingscontrole/app-definition.yaml#L306)
to be flipped to `everyone` and the `Appsemble Login` method to be disabled.

```yaml
security:
  default:
    role: BOA
    # TODO: make sure to update the policy to everyone and remove Appsemble login when put into production,
    # see "important" note https://appsemble.app/en/docs/03-guide/security#security-definition.
    policy: organization
  roles: ...
```

### Certificates and Secrets

**Pro6pp Secret**

The app requires a [Pro6PP](https://www.pro6pp.nl/) secret to autocomplete address information, only
by entering the postal code and street number. The secret can be configured as
[Query Parameter](https://appsemble.app/en/docs/03-guide/service#query-parameter) in the app's
`Service Secrets` with the parameter name being `authKey`.

**SSL Certicate (may be used in production)**

```sh
openssl req -new -newkey rsa:4096 -nodes -keyout sittard-geleen.nl.key -out sittard-geleen.nl.csr -subj "/C=NL/ST=Limburg/L=Sittard-Geleen/O=Gemeente Sittard-Geleen/OU=IT/CN=sittard-geleen.nl"
```

**Mutual-TLS x509 Certificate**

The app requires an `x509` certificate to be generated in `PEM` format. The public certificate
should be trusted in the backend of the party trusting Appsemble. The public certificate and private
key should be set as
[Client Certificate](https://appsemble.app/en/docs/03-guide/service#client-certificate) in the app's
`Service Secrets`.

The private key and public certificate can be created using the following command:

```sh
openssl req -new -x509 -newkey rsa:4096 -nodes -keyout sittard-geleen.key -out pub.pem -days 365 -subj "/C=NL/ST=Limburg/L=Sittard-Geleen/O=Gemeente Sittard-Geleen/OU=IT/CN=sittard-geleen.nl"
```

For more information refer to: <https://appsemble.app/en/docs/03-guide/service#client-certificate>
