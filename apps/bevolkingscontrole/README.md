The Population Control App is a tool designed for Special Investigation Officers (BOAs) of the
municipality of Sittard-Geleen. The purpose of the app is to help BOAs check unexpected and illegal
activities at suspicious addresses.

The app is divided into three main parts, each assigned to a specific role: two for the Admin and
one for the BOA.

When opening the Population Control App, you will first arrive at the 'openChecks' page. This page,
visible to both the BOAs and the Admins, shows all addresses where checks are currently taking
place. Depending on the user's role, certain blocks on this page are visible or hidden.

Both the BOAs and the Admins can proceed from the 'openChecks' page to the 'checksAtAddress' page.
This page provides an overview of the current controls at the selected address. Admins have the
option to delete or update controls here, while BOAs can start.

After these steps, the following pages are specific to each role.

In accordance with the GDPR, this app can be used in two ways: the data can be stored at Appsemble,
or the data can be stored on its own data source, where the storage on the Appsemble infrastructure
is skipped. This offers flexibility and control over where and how the data is stored.
