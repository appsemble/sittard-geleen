# Development

### Table of contents

- [Description](#description)
- [Development](#development)
- [Testing](#testing)
- [Tips](#tips)
- [FLows](#flows)
- [References](#references)

### Description

The document describes the information relevant to development of the app.

### Development

In local development the following may need to be changed in the Appsemble proxy to resolve the
correct IP-address, otherwise the proxy will timeout trying to resolve an Ipv6 address.

_Make sure to restart the server when applied._

- [First https.Agent config](https://gitlab.com/appsemble/appsemble/-/blob/18330a2ebabd88e1995cd515aaef6fe9a89c7d14/packages/server/options/applyAppServiceSecrets.ts#L120)
- [Second https.Agent config](https://gitlab.com/appsemble/appsemble/-/blob/18330a2ebabd88e1995cd515aaef6fe9a89c7d14/packages/server/options/applyAppServiceSecrets.ts#L141)

```typescript
newAxiosConfig.httpsAgent = new https.Agent({
  cert: serviceSecret.identifier,
  key: decryptedSecret,
  // Needed for local development
  family: 4,
});
```

### Testing

The app will be tested with End-To-End tests (in this case mocking the backend), to ensure the
[flows](#flows) as described, will keep working. The tests use Playwright and can be found in the
[packages/e2e/apps/bevolkingscontrole](packages/e2e/apps/bevolkingscontrole) directory.

### Tips

- **Translations** - There are currently only Dutch translations present. When updating the block
  version
  [here](https://gitlab.com/appsemble/apps/sittard-geleen/-/blob/386b17bfe2de496d7262ed24f7f4286007546dae/apps/bevolkingscontrole/app-definition.yaml#L6),
  make sure to also update the
  [version](https://gitlab.com/appsemble/apps/sittard-geleen/-/blob/main/apps/bevolkingscontrole/i18n/nl.json?ref_type=heads#L24)
  in the translations.
- **History remapper** - The app makes heavy use of the
  [history remapper](https://appsemble.app/en/docs/remapper/history), so knowing this remapper well
  is important to understand the app.
- **Shared code** - The app shares quite a bit of logic using anchors which can be found
  [here](https://gitlab.com/appsemble/apps/sittard-geleen/-/blob/386b17bfe2de496d7262ed24f7f4286007546dae/apps/bevolkingscontrole/app-definition.yaml#L5)
- **Resources not used** - The resources are currently not being used there're no resource actions
  using them. However for the large part they should represent what is present in the database.
- **"booleanLike" in resources** - The resources contain this
  [booleanLike](https://gitlab.com/appsemble/apps/sittard-geleen/-/blob/386b17bfe2de496d7262ed24f7f4286007546dae/apps/bevolkingscontrole/app-definition.yaml#L306),
  since the Oracle Apex database can only store booleans as `0` or `1`, resulting in numbers being
  sent back. However these are simply converted back into booleans within the blocks.
- **To-do comments** -
  [These comments](https://gitlab.com/appsemble/apps/sittard-geleen/-/blob/386b17bfe2de496d7262ed24f7f4286007546dae/apps/bevolkingscontrole/app-definition.yaml#L135)
  refer to an issue, which when implemented (so closed and released) can be used instead.

### Flows

The app currently contains 3 main flows. These flows each belong to a specific role. The first 2
belong to the `Admin` role and the third to the `BOA` role.

**The Bevolkingscontrole app** opens up on the
[openChecks](https://gitlab.com/appsemble/apps/sittard-geleen/-/blob/386b17bfe2de496d7262ed24f7f4286007546dae/apps/bevolkingscontrole/app-definition.yaml#L400)
page. This page is visible to the
[BOA's and Admin's](https://gitlab.com/appsemble/apps/sittard-geleen/-/blob/386b17bfe2de496d7262ed24f7f4286007546dae/apps/bevolkingscontrole/app-definition.yaml#L313)
in the app, with some exceptions to
[what blocks are visible](https://gitlab.com/appsemble/apps/sittard-geleen/-/blob/386b17bfe2de496d7262ed24f7f4286007546dae/apps/bevolkingscontrole/app-definition.yaml#L428).
The page lists all addresses with currently open checks.

For both the BOA's and Admin's the **openChecks** page goes to the
[checksAtAddress](https://gitlab.com/appsemble/apps/sittard-geleen/-/blob/386b17bfe2de496d7262ed24f7f4286007546dae/apps/bevolkingscontrole/app-definition.yaml#L480)
page, which as the name implies links to the current checks at that address. The Admin's can delete
and update the checks here and the BOA's can start them.

From here none of the pages are shared between the BOA's and Admin's.

<div style="display: flex; flex-direction: row">

The
[adminCreateFlow](https://gitlab.com/appsemble/apps/sittard-geleen/-/blob/386b17bfe2de496d7262ed24f7f4286007546dae/apps/bevolkingscontrole/app-definition.yaml#L601)
allows the Admin's to create new check on the
[addCheck](https://gitlab.com/appsemble/apps/sittard-geleen/-/blob/386b17bfe2de496d7262ed24f7f4286007546dae/apps/bevolkingscontrole/app-definition.yaml#L613)
step/sub-page and add people to that check on the
[addPeople](https://gitlab.com/appsemble/apps/sittard-geleen/-/blob/386b17bfe2de496d7262ed24f7f4286007546dae/apps/bevolkingscontrole/app-definition.yaml#L643)
step/sub-page and finish creating from there.

<div style="min-width: 600px">

```mermaid
  graph TD
    A[openChecks] --> B[adminCreateFlow]
    B --> E[addCheck] --> F[addPeople] --> |Finish| A
```

</div>

</div>

<div style="display: flex; flex-direction: row">

The
[adminEditFlow](https://gitlab.com/appsemble/apps/sittard-geleen/-/blob/386b17bfe2de496d7262ed24f7f4286007546dae/apps/bevolkingscontrole/app-definition.yaml#L786)
allows to edit currently open checks through the
[editCheck](https://gitlab.com/appsemble/apps/sittard-geleen/-/blob/386b17bfe2de496d7262ed24f7f4286007546dae/apps/bevolkingscontrole/app-definition.yaml#L799)
step/sub-page and change the people assigned on the
[editPeople](https://gitlab.com/appsemble/apps/sittard-geleen/-/blob/386b17bfe2de496d7262ed24f7f4286007546dae/apps/bevolkingscontrole/app-definition.yaml#L868)
step/sub-page.

<div style="min-width: 600px">

```mermaid
  graph TD
    A[openChecks] --> C[checksAtAddress]
    C --> |Edit| D[adminEditFlow]
    D --> G[editCheck] --> H[editPeople] --> |Finish| A
    C --> |Delete| A
```

</div>

</div>

<div style="display: flex; flex-direction: row">

<div style="display: flex; flex-direction: column">

The BOA's have several flows mixed into one and it all starts on the
[residenceCheck](https://gitlab.com/appsemble/apps/sittard-geleen/-/blob/386b17bfe2de496d7262ed24f7f4286007546dae/apps/bevolkingscontrole/app-definition.yaml#L1011)
page. The first step/sub-page
([check](https://gitlab.com/appsemble/apps/sittard-geleen/-/blob/386b17bfe2de496d7262ed24f7f4286007546dae/apps/bevolkingscontrole/app-definition.yaml#L1024))
is where the BOA's mention whether the door at an address has been opened or not.

**Specifying no** will link to the
[checkDoorClose](https://gitlab.com/appsemble/apps/sittard-geleen/-/blob/386b17bfe2de496d7262ed24f7f4286007546dae/apps/bevolkingscontrole/app-definition.yaml#L1146)
step/sub-page. This page contains reasons for whether there is a reason to suspect that people are
at the address or not. Then the page after that
[recheck](https://gitlab.com/appsemble/apps/sittard-geleen/-/blob/386b17bfe2de496d7262ed24f7f4286007546dae/apps/bevolkingscontrole/app-definition.yaml#L1273),
questions whether a new check should be created to revisit the address, or to unregister the people
at the address from the BRP or to do nothing else.

**Specifying yes** will link to the
[registeredPeople](https://gitlab.com/appsemble/apps/sittard-geleen/-/blob/386b17bfe2de496d7262ed24f7f4286007546dae/apps/bevolkingscontrole/app-definition.yaml#L1395)
step/sub-page. Here the people originally expected to be the residents are listed.

The people listed need to be checked, which can be done on the
[present](https://gitlab.com/appsemble/apps/sittard-geleen/-/blob/386b17bfe2de496d7262ed24f7f4286007546dae/apps/bevolkingscontrole/app-definition.yaml#L1758)
step/sub-page. The page contains the person's name and potentially an additional comment which
could've been entered by the Admin's when creating the check. First you can choose whether the
person is currently present. If the person is present 3 options show up, these options are
selectable, which are "Did this person open the door", "Is this the person's home address" and/or
"Is this the person's mailing address", identified by their icons.

| Combination                                                                   | Options, on the next page                                            |
| ----------------------------------------------------------------------------- | -------------------------------------------------------------------- |
| The person is present                                                         | none, unregisterBRP, returnDecision                                  |
| The address is the person's mailing address and not their residential address | none, registerBRP                                                    |
| All other combinations                                                        | none, unregisterBRP, registerBRP, returnDecision, temporaryResidence |

This step/sub-page then links to the
[followUpAction](https://gitlab.com/appsemble/apps/sittard-geleen/-/blob/386b17bfe2de496d7262ed24f7f4286007546dae/apps/bevolkingscontrole/app-definition.yaml#L1821),
where some options to choose from are listed based on what has been entered on the previous
step/sub-page.

| Option             | Result in app                                                                                                                                                                                                                                                                                                                                 |
| ------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| none               | N.A.                                                                                                                                                                                                                                                                                                                                          |
| unregisterBRP      | The person should be unregistered at the address from the BRP                                                                                                                                                                                                                                                                                 |
| registerBRP        | The person should be registered at the address in the BRP                                                                                                                                                                                                                                                                                     |
| returnDecision     | A follow up check should be created to come back at the same address, this will create the new check when the current one is finished. The people uploaded with that check are everyone previously expected to be at the address, and all unexpected people, except the people that are visiting or the ones being unregistered from the BRP. |
| temporaryResidence | The person should be allowed temporary residence                                                                                                                                                                                                                                                                                              |

When an option has been chosen the app links back to the **registeredPeople** step/sub-page with the
person being checked.

Another option on the **registeredPeople** page is to add new people previously not expected to be
present by the Admin's. The same five follow up actions can be specified `none`, `unregisterBRP`,
`registerBRP`, `returnDecision` and `temporaryResidence`.

When all people have been checked, the next step/sub-page is
[addPhotos](https://gitlab.com/appsemble/apps/sittard-geleen/-/blob/386b17bfe2de496d7262ed24f7f4286007546dae/apps/bevolkingscontrole/app-definition.yaml#L1983)
where the BOA's can add some images from location and continue to the last step.

On the last step
([finalRemarks](https://gitlab.com/appsemble/apps/sittard-geleen/-/blob/386b17bfe2de496d7262ed24f7f4286007546dae/apps/bevolkingscontrole/app-definition.yaml#L2039))
some comments can be added and then the check can be finished, uploading all changes.

</div>

<div style="min-width: 600px">

```mermaid
  graph TD
    A[openChecks] --> C[checksAtAddress]
    C --> D[residenceCheck] --> E[check]
    E --> |No| F[checkDoorClose] --> G[recheck] --> |Finish| A
    E --> |Yes| H[registeredPeople]
    H --> |check| I[present] --> J[followUpAction] --> H
    H --> |continue| K[addPhotos] --> L[finalRemarks] --> |Finish| A
```

</div>

</div>

### References

- [InVision](https://invis.io/HG12FHU8VU4B)
