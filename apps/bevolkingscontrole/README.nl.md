De Bevolkingscontrole App is een hulpmiddel ontworpen voor Speciale Onderzoeksambtenaren (BOA’s) in
de gemeente Sittard-Geleen. Het doel van de app is om BOA’s te helpen bij het controleren van
onverwachte en onwettige activiteiten op verdachte adressen.

De app is opgedeeld in drie hoofdonderdelen, elk toegewezen aan een specifieke rol: twee voor de
Admin en één voor de BOA.

Bij het openen van de Bevolkingscontrole App, komt u eerst op de ‘openChecks’ pagina. Deze pagina,
zichtbaar voor zowel de BOA’s als de Admin’s, toont alle adressen waar momenteel controles
openstaan. Afhankelijk van de rol van de gebruiker zijn bepaalde blokken op deze pagina zichtbaar of
verborgen.

Zowel de BOA’s als de Admin’s kunnen vanaf de ‘openChecks’ pagina doorgaan naar de ‘checksAtAddress’
pagina. Deze pagina geeft een overzicht van de huidige controles op het geselecteerde adres. Admin’s
hebben hier de mogelijkheid om controles te verwijderen of bij te werken, terwijl BOA’s ze kunnen
starten.

Na deze stappen zijn de volgende pagina’s specifiek voor elke rol.

In overeenstemming met de AVG kan deze app op twee manieren worden gebruikt: de data kan worden
opgeslagen bij Appsemble, of de data kan worden opgeslagen op een eigen databron, waarbij de opslag
op de Appsemble infrastructuur wordt overgeslagen. Dit biedt flexibiliteit en controle over waar en
hoe de gegevens worden opgeslagen.
