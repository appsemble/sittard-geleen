# Sittard-Geleen

<img src="./icon.png" width=128>

Appsemble apps for the municipality of Sittard-Geleen, in The Netherlands.

---

## Project structure

### Apps

The Appsemble app definitions are defined in the [apps](./apps/) directory.

### Packages

Packages that define reusable logic, scripts and test are maintained in the [packages](./packages/)
directory.
