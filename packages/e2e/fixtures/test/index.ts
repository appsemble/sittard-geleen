import { test as base, expect } from '@playwright/test';

const {
  BOT_ACCOUNT_EMAIL = 'bot@appsemble.com',
  BOT_ACCOUNT_PASSWORD = 'test',
  ORGANIZATION = 'appsemble',
} = process.env;

interface Fixtures {
  /**
   * Visit an app.
   *
   * Uses to values from the Appsemble class.
   *
   * @param appPath The path of the app to visit.
   */
  visitApp: (appPath: string) => Promise<void>;

  /**
   * Set the role of the user.
   *
   * Note that this requires the user to be logged in to the studio.
   *
   * @param app The app to set the role for.
   * @param role The role to set.
   */
  changeRole: (app: string, role: string) => Promise<void>;

  /**
   * Set the role of the user in a team.
   *
   * @param app The app to set the role for.
   * @param team The team to set the role for.
   * @param role The role to set.
   */
  changeTeamRole: (app: string, team: string, role: 'manager' | 'member') => Promise<void>;

  /**
   * Login to the Appsemble studio.
   */
  studioLogin: () => Promise<void>;

  /**
   * Perform a logout in Appsemble Studio.
   */
  studioLogout: () => Promise<void>;

  /**
   * Login to an Appsemble demo app.
   *
   * @param role The role to login in as.
   */
  loginDemoApp: (role: string) => Promise<void>;

  /**
   * Reseed a demo app.
   *
   * @param app The app to reseed.
   */
  reseedDemoApp: (app: string) => Promise<void>;

  /**
   * Create resource(s) from JSON input.
   *
   * @param appPath The app path.
   * @param type The resource type.
   * @param input The resource JSON input.
   */
  createResource: (appPath: string, type: string, input: unknown) => Promise<unknown>;
}

export const test = base.extend<Fixtures>({
  async visitApp({ baseURL, page }, use) {
    await use(async (appPath: string) => {
      await page.goto(
        `http://${appPath}.${ORGANIZATION}.${new URL(baseURL ?? 'http://localhost:9999').host}`,
      );
    });
  },

  async studioLogin({ page }, use) {
    await use(async () => {
      const queryParams = new URLSearchParams({ redirect: '/en/apps' });
      await page.goto(`/en/login?${String(queryParams)}`);

      await page.waitForLoadState('domcontentloaded');
      await page.waitForSelector('.appsemble-loader', { state: 'hidden' });

      await page.getByTestId('email').fill(BOT_ACCOUNT_EMAIL);
      await page.getByTestId('password').fill(BOT_ACCOUNT_PASSWORD);
      await page.getByTestId('login').click();

      await expect(page).toHaveURL('/en/apps');
    });
  },

  async studioLogout({ page }, use) {
    await use(async () => {
      await page
        .locator(
          'div.navbar-item.has-dropdown.is-right > button.navbar-link:has(img[alt="Profile Picture"])',
        )
        .click();
      await page.getByRole('button', { name: 'Logout' }).click();
    });
  },

  async changeRole({ page }, use) {
    await use(async (app: string, role: string) => {
      const redirect = page.url();
      await page.goto(`/en/organizations/${ORGANIZATION}`);
      await page.click(`text=${app}`);
      const appId = /\/\w+\/apps\/(\d+)\//.exec(page.url())?.[1];
      await page.goto(`/en/apps/${appId}/-/users`);
      const select = page.locator('tr', { hasText: 'It’s you!' }).locator('select[class=""]');
      await select.selectOption(role);
      await page.goto(redirect);
    });
  },

  async changeTeamRole({ page }, use) {
    await use(async (app: string, team: string, role: 'manager' | 'member') => {
      const redirect = page.url();
      await page.goto(`/en/organizations/${ORGANIZATION}`);
      await page.click(`text=${app}`);
      const appId = /\/\w+\/apps\/(\d+)\//.exec(page.url())?.[1];
      await page.goto(`/en/apps/${appId}/-/teams`);
      await page.click(`text=${team}`);
      await page.getByRole('row', { name: 'Bot' }).locator('#role').selectOption(role);
      await page.goto(redirect);
    });
  },

  async loginDemoApp({ page }, use) {
    await use(async (role: string) => {
      await page.waitForLoadState('domcontentloaded');
      await page.waitForSelector('.appsemble-loader', { state: 'hidden' });

      const btn = page.getByTestId('create-account');
      if (await btn.isVisible()) {
        await page.getByTestId('app-role').selectOption(role);
        await btn.click();
      }
    });
  },

  async reseedDemoApp({ page }, use) {
    await use(async (app: string) => {
      await page.goto(`/en/organizations/${ORGANIZATION}`);
      await page.click(`text=${app}`);
      const appId = /\/\w+\/apps\/(\d+)\//.exec(page.url())?.[1];
      await page.goto(`/en/apps/${appId}/-/`);
      await page.getByRole('button', { name: 'Reseed App' }).click();
      await page.getByRole('button', { name: 'Reseed', exact: true }).click();
    });
  },

  async createResource({ baseURL, page }, use) {
    await use(async (appPath: string, type: string, input: unknown) => {
      const apps = await fetch(`${baseURL}/api/apps`).then<
        {
          id: number;
          path: string;
        }[]
      >((res) => res.json());
      expect(apps).toBeInstanceOf(Array);
      const appId = apps.find((app) => app.path === appPath)?.id;
      expect(appId).not.toBeUndefined();
      await page.waitForLoadState('domcontentloaded');
      await page.waitForSelector('.appsemble-loader', { state: 'hidden' });
      await page.waitForTimeout(1000);
      const accessToken = await page.evaluate(() =>
        Promise.resolve(localStorage.getItem('refresh_token')!),
      );
      expect(accessToken).toStrictEqual(expect.any(String));
      const response = await fetch(`${baseURL}/api/apps/${appId}/resources/${type}`, {
        method: 'POST',
        body: JSON.stringify(input),
        headers: {
          Authorization: `Bearer ${accessToken}`,
          'Content-Type': 'application/json',
        },
      });

      return response.json();
    });
  },
});
