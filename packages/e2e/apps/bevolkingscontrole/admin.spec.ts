import { expect } from '@playwright/test';

import { test } from '../../fixtures/test/index.js';

const appPath = 'bevolkingscontrole';

test.describe('Admin flow', () => {
  test.beforeEach(async ({ loginDemoApp, reseedDemoApp, studioLogin, visitApp }) => {
    await studioLogin();
    await reseedDemoApp(appPath);
    await visitApp(appPath);
    await loginDemoApp('Admin');
  });

  test('should delete check', async ({ page }) => {
    await expect(page.getByRole('heading', { name: 'Openstaande controles' })).toBeVisible();

    await page
      .getByRole('row', { name: /Kollenberg-Park Leyenbroek Hub Dassenplein 1/ })
      .getByRole('button')
      .click();
    await page.getByRole('button', { name: 'Bekijken' }).click();

    await expect(page.getByRole('link', { name: 'Controle' })).toBeVisible();
    await expect(
      page.getByText('Hub Dassenplein 1 6131 LB Sittard Kollenberg-Park Leyenbroek'),
    ).toBeVisible();
    await expect(page.getByRole('row')).toContainText([/ActieDatum/, /Bevolkingscontrole/]);

    await page.getByRole('row').locator('.dropdown').last().click();
    await page.getByRole('button', { name: 'Verwijderen' }).click();

    await expect(page.getByRole('heading', { name: 'Openstaande controles' })).toBeVisible();
    await expect(page.getByText('Er zijn geen gegevens beschikbaar.')).toBeVisible();
  });

  test('should create check', async ({ page }) => {
    await expect(page.getByRole('heading', { name: 'Openstaande controles' })).toBeVisible();
    await page.getByRole('link', { name: 'Nieuwe controle toevoegen' }).click();
    await page.getByRole('combobox', { name: 'Prioriteit taak' }).selectOption('Normaal');

    await page.route('/api/apps/*/actions/pages.*.actions.pro6pp*', (route) => {
      route.fulfill({ path: 'fixtures/data/pro6pp.json' });
    });
    await expect(page.getByRole('heading', { name: 'Controle toevoegen' })).toBeVisible();
    await page.getByPlaceholder('0000 AA').fill('0000 AA');
    await page.getByPlaceholder('0', { exact: true }).fill('1');
    await expect(page.getByLabel('Straat')).toHaveValue('Straat');
    await expect(page.getByLabel('Woonplaats')).toHaveValue('Woonplaats');
    await expect(page.getByLabel('Buurt')).toHaveValue('Buurt');
    await page.getByLabel('Opmerking(Optioneel)').fill('Opmerking');
    await page.getByRole('button', { name: 'Volgende stap' }).click();

    await expect(page.getByRole('heading', { name: 'Personen invoeren' })).toBeVisible();
    await page.getByLabel('Naam, Voorletters, Achternaam').fill('Persoon');
    await page.getByRole('textbox', { name: 'DD-MM-JJJJ' }).fill('01-01-2024');
    await page.getByRole('textbox', { name: 'DD-MM-JJJJ' }).press('Enter');
    await page.getByRole('button', { name: 'Persoon toevoegen' }).click();
    await expect(page.getByRole('row')).toContainText([/Geb.datumNaam/, /01-01-2024Persoon/]);

    await page.getByLabel('Naam, Voorletters, Achternaam').fill('Persoon aanpassen');
    await page.getByRole('textbox', { name: 'DD-MM-JJJJ' }).fill('01-01-2024');
    await page.getByRole('textbox', { name: 'DD-MM-JJJJ' }).press('Enter');
    await page.getByRole('button', { name: 'Persoon toevoegen' }).click();
    await expect(page.getByRole('row')).toContainText([
      /Geb.datumNaam/,
      /01-01-2024Persoon/,
      /01-01-2024Persoon aanpassen/,
    ]);

    await page
      .getByRole('row', { name: /01-01-2024 Persoon aanpassen/ })
      .getByRole('button')
      .click();
    await page.getByRole('button', { name: 'Bewerken' }).click();
    await page.getByLabel('Naam, Voorletters, Achternaam').fill('Persoon is aangepast');
    await page.getByRole('textbox', { name: 'DD-MM-JJJJ' }).fill('01-01-2024');
    await page.getByRole('textbox', { name: 'DD-MM-JJJJ' }).press('Enter');
    await page.getByLabel('Opmerking(Optioneel)').fill('Opmerking toegevoegd');
    await page.getByRole('button', { name: 'Persoon toevoegen' }).click();
    await expect(page.getByRole('row')).toContainText([
      /Geb.datumNaam/,
      /01-01-2024Persoon/,
      /01-01-2024Persoon is aangepast/,
    ]);

    await page.getByLabel('Naam, Voorletters, Achternaam').fill('Persoon verwijderen');
    await page.getByRole('textbox', { name: 'DD-MM-JJJJ' }).fill('01-01-2024');
    await page.getByRole('textbox', { name: 'DD-MM-JJJJ' }).press('Enter');
    await page.getByRole('button', { name: 'Persoon toevoegen' }).click();
    await expect(page.getByRole('row')).toContainText([
      /Geb.datumNaam/,
      /01-01-2024Persoon/,
      /01-01-2024Persoon is aangepast/,
      /01-01-2024Persoon verwijderen/,
    ]);

    await page
      .getByRole('row', { name: /01-01-2024 Persoon verwijderen/ })
      .getByRole('button')
      .click();
    await page.getByRole('button', { name: 'Verwijderen' }).click();
    await expect(page.getByRole('row')).toContainText([
      /Geb.datumNaam/,
      /01-01-2024Persoon/,
      /01-01-2024Persoon is aangepast/,
    ]);

    await page.getByRole('button', { name: 'Controle toevoegen' }).click();
    await expect(page.getByRole('heading', { name: 'Openstaande controles' })).toBeVisible();
    await expect(page.getByRole('row')).toContainText([/BuurtStraat/, /BuurtStraat 1/]);
  });

  test('should edit check', async ({ browserName, createResource, page }) => {
    // Really flaky
    if (browserName === 'chromium') {
      test.skip();
    }
    const response = (await createResource(appPath, 'residenceCheck', {
      priority: 'normal',
      postalCode: '0000 AA',
      houseNumber: '1',
      addition: '',
      streetName: 'Straat',
      residenceArea: 'Woonplaats',
      neighbourhood: 'Buurt',
      completed: false,
      subType: 'residenceCheck',
      officeRemarks: 'Opmerking',
    })) as Record<string, unknown>;
    await createResource(appPath, 'person', [
      {
        residenceCheckId: response.id,
        fullName: 'Persoon aanpassen',
        birthdate: '2024-01-01',
      },
      {
        residenceCheckId: response.id,
        fullName: 'Persoon',
        birthdate: '2024-01-01',
      },
    ]);

    await page.waitForLoadState('domcontentloaded');
    await page.waitForSelector('.appsemble-loader', { state: 'hidden' });
    await page.waitForTimeout(1000);
    await page.reload();

    await expect(page.getByRole('heading', { name: 'Openstaande controles' })).toBeVisible();
    await expect(page.getByRole('row')).toContainText([/BuurtStraat/, /BuurtStraat 1/]);
    await page
      .getByRole('row', { name: /Buurt Straat 1/ })
      .getByRole('button')
      .click();
    await page.getByRole('button', { name: 'Bekijken' }).click();

    await expect(page.getByRole('link', { name: 'Controle' })).toBeVisible();
    await expect(page.getByText('Straat 1 0000 AA Woonplaats Buurt')).toBeVisible();

    await page.getByRole('row').locator('.dropdown').first().click();
    await page.getByRole('button', { name: 'Bewerken' }).click();

    await expect(page.getByRole('heading', { name: 'Controle aanpassen' })).toBeVisible();
    await expect(page.getByRole('combobox', { name: 'Prioriteit taak' })).toHaveValue('0');
    await expect(page.getByLabel('Postcode')).toHaveValue('0000 AA');
    await expect(page.getByLabel('Huisnummer')).toHaveValue('1');
    await expect(page.getByLabel('Straat')).toHaveValue('Straat');
    await expect(page.getByLabel('Woonplaats')).toHaveValue('Woonplaats');
    await expect(page.getByLabel('Buurt')).toHaveValue('Buurt');
    await expect(page.getByLabel('Opmerking(Optioneel)')).toHaveValue('Opmerking');
    await page.getByRole('button', { name: 'Volgende stap' }).click();

    await expect(page.getByRole('heading', { name: 'Personen invoeren' })).toBeVisible();
    await expect(page.getByRole('row')).toContainText([
      /Geb.datumNaam/,
      /01-01-2024Persoon/,
      /01-01-2024Persoon aanpassen/,
    ]);

    await page
      .getByRole('row', { name: /01-01-2024 Persoon aanpassen/ })
      .getByRole('button')
      .click();
    await page.getByRole('button', { name: 'Bewerken' }).click();
    await page.getByLabel('Naam, Voorletters, Achternaam').fill('Persoon is aangepast');
    await page.getByRole('textbox', { name: 'DD-MM-JJJJ' }).fill('02-01-2000');
    await page.getByRole('textbox', { name: 'DD-MM-JJJJ' }).press('Enter');
    await page.getByLabel('Opmerking(Optioneel)').fill('Opmerking toegevoegd');
    await page.getByRole('button', { name: 'Persoon toevoegen' }).click();

    await expect(page.getByRole('row')).toContainText([
      /Geb.datumNaam/,
      /01-01-2024Persoon/,
      /02-01-2000Persoon is aangepast/,
    ]);

    await page.getByRole('button', { name: 'Controle aanpassen' }).click();
    await expect(page.getByRole('heading', { name: 'Openstaande controles' })).toBeVisible();
    await expect(page.getByRole('row')).toContainText([/BuurtStraat/, /BuurtStraat 1/]);
  });
});
