import { expect, type Page } from '@playwright/test';
import { addDays, format } from 'date-fns';

import { test } from '../../fixtures/test/index.js';

const appPath = 'bevolkingscontrole';

function getFutureDate(numOfDays = 7): string {
  return format(addDays(new Date(), numOfDays), 'dd-MM-yyyy');
}

interface Person {
  fullName: string;
  birthdate: string;
  remarks?: string;
  present: 'Ja' | 'Nee';
  options?: ('Briefadres' | 'Heeft de deur geopend' | 'Woonadres')[];
  action:
    | 'Bezoekerscontrole'
    | 'Geen actie, bewoning klopt'
    | 'Inschrijven BRP'
    | 'Nee'
    | 'Terugkeerbesluit'
    | 'Uitschrijven BRP'
    | 'Verblijf max 3/4 maanden';
  boaRemark?: string;
}

async function assertPersonInfo(page: Page, person: Person): Promise<void> {
  await expect(page.getByRole('heading', { name: person.fullName })).toBeVisible();
  await expect(page.getByText(person.birthdate)).toBeVisible();
  await expect(page.getByText(person.remarks ?? '')).toBeVisible();
}

async function assertRegisteredPeopleStep(
  page: Page,
  people: Person[],
  currentIndex: number,
): Promise<void> {
  await expect(page.getByRole('heading', { name: 'Personen ingeschreven BRP' })).toBeVisible();
  await expect(page.getByText('Straat 1 0000 AA Woonplaats Buurt')).toBeVisible();
  await expect(page.getByRole('row')).toContainText([
    /Geb.datumNaam/,
    ...people.map(
      (person, index) =>
        new RegExp(
          [
            person.birthdate,
            person.fullName,
            currentIndex > index ? (person.present === 'Ja' ? '✓' : 'x') : undefined,
          ].join(''),
        ),
    ),
  ]);
}

async function assertPresentStep(page: Page, person: Person): Promise<void> {
  await page
    .getByRole('row', { name: new RegExp([person.birthdate, person.fullName].join(' ')) })
    .click();
  await assertPersonInfo(page, person);
  await page.getByLabel('Is de bewoner aanwezig?').selectOption(person.present);
  for (const option of person.options ?? []) {
    await page.locator('label').filter({ hasText: option }).locator('i').click();
  }
  await page.getByRole('button', { name: 'Volgende stap' }).click();
}

async function assertFollowUpStep(page: Page, person: Person): Promise<void> {
  await assertPersonInfo(page, person);
  if (
    !(
      person.action === 'Geen actie, bewoning klopt' &&
      (await page.getByLabel('Vervolgactie').isDisabled())
    )
  ) {
    await page.getByLabel('Vervolgactie').selectOption(person.action);
    if (person.action === 'Terugkeerbesluit' || person.action === 'Verblijf max 3/4 maanden') {
      await page.getByRole('textbox', { name: 'DD-MM-JJJJ' }).fill(getFutureDate());
      await page.getByRole('textbox', { name: 'DD-MM-JJJJ' }).press('Enter');
    }
  }
  await page.getByLabel('Opmerking(Optioneel)').fill(person.boaRemark ?? '');
  await page.getByRole('button', { name: 'Volgende stap' }).click();
}

async function addPerson(
  page: Page,
  person: Person & { reason?: 'Bezoek' | 'Geen' },
): Promise<void> {
  await page.getByRole('button', { name: 'Persoon toevoegen' }).click();
  await page.getByLabel('Naam, Voorletters, Achternaam').fill(person.fullName);
  await page.getByRole('textbox', { name: 'DD-MM-JJJJ' }).fill(person.birthdate);
  await page.getByRole('textbox', { name: 'DD-MM-JJJJ' }).press('Enter');
  await page.getByLabel('Reden aanwezigheid').selectOption(person.reason ?? 'Geen');
  await expect(page.getByLabel('Reden aanwezigheid')).toContainText(person.reason ?? 'Geen');
  await page.getByLabel('Vervolgactie').selectOption(person.action);
  if (person.action === 'Bezoekerscontrole') {
    await page.route('/api/apps/*/actions/pages.*.actions.pro6pp*', (route) => {
      route.fulfill({ path: 'fixtures/data/pro6pp.json' });
    });
    await page.getByLabel('Postcode').fill('0000 AA');
    await page.getByLabel('Huisnummer', { exact: true }).fill('1');
  }
  await page.locator('form').getByRole('button', { name: 'Persoon toevoegen' }).click();
  await expect(page.getByText('Nieuw persoon toevoegen')).toBeHidden();
}

test.describe('Residence check flow', () => {
  test.beforeEach(async ({ loginDemoApp, reseedDemoApp, studioLogin, visitApp }) => {
    await studioLogin();
    await reseedDemoApp(appPath);
    await visitApp(appPath);
    await loginDemoApp('BOA');
  });

  test('should support door not opened and rechecks', async ({
    browserName,
    createResource,
    page,
  }) => {
    await createResource(appPath, 'residenceCheck', {
      priority: 'normal',
      postalCode: '0000 AA',
      houseNumber: '1',
      addition: '',
      streetName: 'Straat',
      residenceArea: 'Woonplaats',
      neighbourhood: 'Buurt',
      completed: false,
      subType: 'residenceCheck',
    });

    await page.waitForLoadState('domcontentloaded');
    await page.waitForSelector('.appsemble-loader', { state: 'hidden' });
    await page.waitForTimeout(1000);
    await page.reload();

    if (browserName === 'webkit') {
      test.setTimeout(120_000);
    }
    await expect(page.getByRole('heading', { name: 'Openstaande controles' })).toBeVisible();
    await page.getByRole('row', { name: /Buurt Straat 1/ }).click();

    await expect(page.getByRole('link', { name: 'Controle' })).toBeVisible();
    await expect(page.getByText('Straat 1 0000 AA Woonplaats Buurt')).toBeVisible();
    await expect(page.getByRole('row')).toContainText([/ActieDatum/, /Bevolkingscontrole/]);
    await page.getByRole('button', { name: 'Uitvoeren' }).click();

    await expect(page.getByRole('heading', { name: 'Bevolkingscontrole' })).toBeVisible();
    await expect(page.getByText('Straat 1 0000 AA Woonplaats Buurt')).toBeVisible();
    await expect(page.getByText(/Datum:/)).toBeVisible();
    await expect(page.getByText(/Tijd:/)).toBeVisible();
    await expect(page.getByText(/Boa aanwezig:/)).toBeVisible();
    // eslint-disable-next-line playwright/no-conditional-in-test
    if (await page.getByPlaceholder('Uw naam').isVisible()) {
      await page.getByPlaceholder('Uw naam').fill('bot');
    }
    await page.getByLabel('Overige aanwezigen(Optioneel)').fill('boa2');
    await page.getByLabel('Wordt er opengedaan?').selectOption('Nee');
    await page.getByLabel('Opmerking(Optioneel)').fill('Deur dicht');
    await page.getByRole('button', { name: 'Volgende stap' }).click();

    await page.getByLabel('Lijkt er sprake van bewoning?').selectOption('Ja');
    await page.getByText('Kinderen aanwezig').click();
    await page.getByText('Volwassene aanwezig').click();
    await page.getByText('Huisdieren aanwezig').click();
    await page.getByText('Licht/tv/muziek aan').click();
    await page.getByLabel('Anders, namelijk(Optioneel)').fill('tv is aan');
    await page.getByRole('button', { name: 'Volgende stap' }).click();

    await page.getByLabel('Vervolgactie(Optioneel)').selectOption('Hercontrole');
    await page.getByRole('textbox', { name: 'DD-MM-JJJJ' }).fill(getFutureDate());
    await page.getByRole('textbox', { name: 'DD-MM-JJJJ' }).press('Enter');
    await page.getByLabel('Opmerking(Optioneel)').fill('Opmerking BOA');
    await page.getByRole('button', { name: 'Controle verzenden' }).click();

    // Recheck
    await expect(page.getByRole('heading', { name: 'Openstaande controles' })).toBeVisible();
    await page.getByRole('row', { name: /Buurt Straat 1/ }).click();

    await expect(page.getByRole('link', { name: 'Controle' })).toBeVisible();
    await expect(page.getByText('Straat 1 0000 AA Woonplaats Buurt')).toBeVisible();
    await expect(page.getByRole('row')).toContainText([/ActieDatum/, /Hercontrole/]);
    await page.getByRole('button', { name: 'Uitvoeren' }).click();

    await expect(page.getByRole('heading', { name: 'Hercontrole' })).toBeVisible();
    await expect(page.getByText('Straat 1 0000 AA Woonplaats Buurt')).toBeVisible();
    await expect(page.getByText(/Datum:/)).toBeVisible();
    await expect(page.getByText(/Tijd:/)).toBeVisible();
    await expect(page.getByText(/Boa aanwezig:/)).toBeVisible();
    // eslint-disable-next-line playwright/no-conditional-in-test
    if (await page.getByPlaceholder('Uw naam').isVisible()) {
      await page.getByPlaceholder('Uw naam').fill('bot');
    }
    await page.getByLabel('Wordt er opengedaan?').selectOption('Nee');
    await page.getByLabel('Opmerking(Optioneel)').click();
    await page.getByLabel('Opmerking(Optioneel)').fill('Niemand thuis');
    await page.getByRole('button', { name: 'Volgende stap' }).click();

    await page.getByLabel('Lijkt er sprake van bewoning?').selectOption('Nee');
    await page.getByText('Ontbreken meubilair').click();
    await page.getByText('Volgens buren onbewoond').click();
    await page.getByLabel('Anders, namelijk(Optioneel)').fill('Geen personen');
    await page.getByRole('button', { name: 'Volgende stap' }).click();

    await page.getByLabel('Vervolgactie(Optioneel)').selectOption('Uitschrijven BRP');
    await page.getByLabel('Opmerking(Optioneel)').fill('De personen moeten uitgeschreven worden.');

    await page.getByRole('button', { name: 'Controle verzenden' }).click();
    await expect(page.getByText(/Buurt Straat 1/)).toBeHidden();
  });

  test('should support door opened', async ({ browserName, createResource, page }) => {
    if (browserName === 'webkit') {
      test.setTimeout(240_000);
    } else if (browserName === 'chromium') {
      test.skip();
      // Test.setTimeout(100_000);
    }

    const people: Person[] = [
      {
        fullName: 'Persoon1',
        birthdate: '01-01-2000',
        remarks: 'Verwacht bewoning klopt',
        present: 'Ja',
        options: ['Heeft de deur geopend', 'Woonadres'],
        action: 'Geen actie, bewoning klopt',
        boaRemark: 'Bewoning klopt inderdaad',
      },
      {
        fullName: 'Persoon2',
        birthdate: '02-01-2000',
        remarks: 'Verwacht briefadres',
        present: 'Ja',
        options: ['Briefadres'],
        action: 'Inschrijven BRP',
        boaRemark: 'Inderdaad het briefadres van persoon',
      },
      {
        fullName: 'Persoon3',
        birthdate: '03-01-2000',
        remarks: 'Verwacht uitgeschreven te worden',
        present: 'Ja',
        options: ['Woonadres'],
        action: 'Uitschrijven BRP',
        boaRemark: 'Inderdaad uitgeschreven',
      },
      {
        fullName: 'Persoon4',
        birthdate: '04-01-2000',
        remarks: 'Verwacht tijdelijk verblijf',
        present: 'Ja',
        options: ['Woonadres'],
        action: 'Verblijf max 3/4 maanden',
        boaRemark: 'Inderdaad tijdelijk verblijf',
      },
      {
        fullName: 'Persoon5',
        birthdate: '05-01-2000',
        remarks: 'Verwacht niet aanwezig',
        present: 'Nee',
        action: 'Terugkeerbesluit',
        boaRemark: 'Inderdaad niet aanwezig',
      },
    ];

    const response = (await createResource(appPath, 'residenceCheck', {
      priority: 'normal',
      postalCode: '0000 AA',
      houseNumber: '1',
      addition: '',
      streetName: 'Straat',
      residenceArea: 'Woonplaats',
      neighbourhood: 'Buurt',
      completed: false,
      subType: 'residenceCheck',
    })) as Record<string, unknown>;
    await createResource(
      appPath,
      'person',
      people.map((p) => ({
        ...p,
        residenceCheckId: response.id,
        birthdate: format(new Date(p.birthdate), 'yyyy-dd-MM'),
        action: undefined,
        options: undefined,
        present: undefined,
        boaRemark: undefined,
      })),
    );
    await page.waitForLoadState('domcontentloaded');
    await page.waitForSelector('.appsemble-loader', { state: 'hidden' });
    await page.waitForTimeout(1000);
    await page.reload();

    await expect(page.getByRole('heading', { name: 'Openstaande controles' })).toBeVisible();
    await page.getByRole('row', { name: /Buurt Straat 1/ }).click();

    await expect(page.getByRole('link', { name: 'Controle' })).toBeVisible();
    await expect(page.getByText('Straat 1 0000 AA Woonplaats Buurt')).toBeVisible();
    await expect(page.getByRole('row')).toContainText([/ActieDatum/, /Bevolkingscontrole/]);
    await page.getByRole('button', { name: 'Uitvoeren' }).click();

    await expect(page.getByRole('heading', { name: 'Bevolkingscontrole' })).toBeVisible();
    await expect(page.getByText('Straat 1 0000 AA Woonplaats Buurt')).toBeVisible();
    await expect(page.getByText(/Datum:/)).toBeVisible();
    await expect(page.getByText(/Tijd:/)).toBeVisible();
    await expect(page.getByText(/Boa aanwezig:/)).toBeVisible();
    // eslint-disable-next-line playwright/no-conditional-in-test
    if (await page.getByPlaceholder('Uw naam').isVisible()) {
      await page.getByPlaceholder('Uw naam').fill('bot');
    }
    await page.getByLabel('Overige aanwezigen(Optioneel)').fill('boa2');
    await page.getByLabel('Wordt er opengedaan?').selectOption('Ja');
    await page.getByLabel('Opmerking(Optioneel)').fill('Deur opengedaan');
    await page.getByRole('button', { name: 'Volgende stap' }).click();

    await page.getByRole('button', { name: 'Volgende stap' }).click();
    await expect(page.getByText('Niet alle personen zijn gecontroleerd')).toBeVisible();

    for (const [index, person] of people.reverse().entries()) {
      await assertRegisteredPeopleStep(page, people, index);
      await assertPresentStep(page, person);
      await assertFollowUpStep(page, person);
    }
    await assertRegisteredPeopleStep(page, people, 5);

    const unexpectedPeople: (Person & { reason?: 'Bezoek' | 'Geen' })[] = [
      {
        fullName: 'Onverwachte persoon1',
        birthdate: '01-01-2000',
        present: 'Ja',
        reason: 'Geen',
        action: 'Nee',
      },
      {
        fullName: 'Onverwachte persoon2',
        birthdate: '02-01-2000',
        present: 'Ja',
        reason: 'Bezoek',
        action: 'Bezoekerscontrole',
      },
      {
        fullName: 'Onverwachte persoon3',
        birthdate: '03-01-2000',
        present: 'Ja',
        reason: 'Geen',
        action: 'Inschrijven BRP',
      },
      {
        fullName: 'Onverwachte persoon4',
        birthdate: '04-01-2000',
        present: 'Ja',
        reason: 'Geen',
        action: 'Terugkeerbesluit',
      },
      {
        fullName: 'Onverwachte persoon5',
        birthdate: '05-01-2000',
        present: 'Ja',
        reason: 'Geen',
        action: 'Verblijf max 3/4 maanden',
      },
    ];

    for (const [index, person] of unexpectedPeople.entries()) {
      people.push(person);
      await addPerson(page, person);
      await assertRegisteredPeopleStep(page, people, index);
    }

    await page.getByRole('button', { name: 'Volgende stap' }).click();
    await expect(page.getByRole('heading', { name: "Foto's toevoegen" })).toBeVisible();
    await expect(page.getByText('Straat 1 0000 AA Woonplaats Buurt')).toBeVisible();
    await page.getByLabel('').setInputFiles('../../icon.png');
    await page.getByRole('button', { name: 'Volgende stap' }).click();

    await expect(page.getByRole('heading', { name: 'Opmerking toevoegen' })).toBeVisible();
    await expect(page.getByText('Straat 1 0000 AA Woonplaats Buurt')).toBeVisible();
    await page.getByLabel('Opmerking(Optioneel)').fill('Laatste opmerking');
    await page.getByRole('button', { name: 'Controle verzenden' }).click();

    await expect(page.getByRole('heading', { name: 'Openstaande controles' })).toBeVisible();
    await page.getByRole('row', { name: /Buurt Straat 1/ }).click();
    await expect(page.getByRole('link', { name: 'Controle' })).toBeVisible();
    await expect(page.getByRole('row')).toContainText([
      /ActieDatum/,
      /Hercontrole/,
      /Bezoekerscontrole/,
    ]);
  });
});
