import { readFile, writeFile } from 'node:fs/promises';

import { logger } from '@appsemble/node-utils';
import { format, resolveConfig } from 'prettier';
import { parseDocument } from 'yaml';
import { type Argv } from 'yargs';

export const command = 'convert path';
export const description = 'Converts a YAML file to a JSON equivalent.';

/**
 * Useful commands:
 * git diff --staged -- apps/bevolkingscontrole/app-definition.yaml > definition.patch
 */

/**
 * Convert yaml to json.
 *
 * @param path The path to the yaml to convert.
 */
async function convert(path: string): Promise<void> {
  logger.info(`Creating ‘${path.replace('.yaml', '.json')}’`);
  const yaml = await readFile(path, 'utf8');
  const doc = parseDocument(yaml);
  const prettierOptions = (await resolveConfig(path, { editorconfig: true }))!;
  prettierOptions.parser = 'json';
  await writeFile(
    path.replace('.yaml', '.json'),
    await format(JSON.stringify(doc.toJSON()), prettierOptions),
  );
  logger.info(`Succesfully created ‘${path}’`);
}

export function builder(yargs: Argv): Argv<any> {
  return yargs.positional('path', {
    describe: 'The path to the yaml to convert.',
  });
}

export interface ConvertArguments {
  path: string;
}

export async function handler({ path }: ConvertArguments): Promise<void> {
  await convert(path);
}
