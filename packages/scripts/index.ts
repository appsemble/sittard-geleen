import yargs, { type CommandModule } from 'yargs';

import * as convert from './commands/convert.js';
import * as seedAccount from './commands/seed-account.js';

yargs()
  .command(convert as unknown as CommandModule)
  .command(seedAccount as unknown as CommandModule)
  .demandCommand(1)
  .parse(process.argv.slice(2));
